Version 4.3.5
=============

Released: January 10, 2023

Status: Stable

Changes
-------

This version backports a change from version 4.4 beta 1 that fixes deleting
users from the user interface.


Removals
--------

- None


Upgrade process
---------------

.. important::

  If using a direct deployment, Supervisord must be upgraded to version
  4.2.2. See troubleshooting section: :ref:`troubleshooting-version-4.1`


.. include:: partials/upgrade-3.5-4.0.txt


Troubleshooting
---------------

Follow the solutions outlined in the troubleshooting section:
:ref:`troubleshooting-version-4.1`


Backward incompatible changes
-----------------------------

- None

Deprecations
------------

- None


Issues closed
-------------

- :gitlab-issue:`1125` Error deleting user

.. _PyPI: https://pypi.python.org/pypi/mayan-edms/
