# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Ingo Buchholz <ingo.buchholz@takwa.de>, 2023
# Roberto Rosario, 2023
# Thomas Lauterbach <lauterbachthomas@googlemail.com>, 2023
# Jesaja Everling <jeverling@gmail.com>, 2023
# Marvin Haschker <marvin@haschker.me>, 2023
# Berny <berny@bernhard-marx.de>, 2023
# Mathias Behrle <mathiasb@m9s.biz>, 2023
# Felix <felix.com@gmx.com>, 2023
# 
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-16 23:30+0000\n"
"PO-Revision-Date: 2023-01-05 02:51+0000\n"
"Last-Translator: Felix <felix.com@gmx.com>, 2023\n"
"Language-Team: German (Germany) (https://www.transifex.com/rosarior/teams/13584/de_DE/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de_DE\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps.py:24 links.py:38 permissions.py:6
msgid "Dependencies"
msgstr "Abhängigkeiten"

#: apps.py:32 apps.py:69 apps.py:78
msgid "Label"
msgstr "Bezeichner"

#: apps.py:36
msgid "Internal name"
msgstr "Interner Name"

#: apps.py:41 apps.py:74 apps.py:83
msgid "Description"
msgstr "Beschreibung"

#: apps.py:45 classes.py:218
msgid "Type"
msgstr "Typ"

#: apps.py:49 classes.py:220
msgid "Other data"
msgstr "Andere Daten"

#: apps.py:53
msgid "Declared by"
msgstr "Deklariert von"

#: apps.py:57 classes.py:218
msgid "Version"
msgstr "Version"

#: apps.py:61
msgid "Environment"
msgstr "Umgebung"

#: apps.py:64 classes.py:220
msgid "Check"
msgstr "Überprüfen"

#: classes.py:218
msgid "Name"
msgstr "Name"

#: classes.py:219
msgid "App"
msgstr "App"

#: classes.py:219 classes.py:911
msgid "Environments"
msgstr "Umgebungen"

#: classes.py:316
msgid "Need to specify at least one: app_label or module."
msgstr ""
"Es muss wenigstens eines von beiden angegeben werden: app_label oder Modul."

#: classes.py:321
#, python-format
msgid "Dependency \"%s\" already registered."
msgstr "Abhängigkeit \"%s\" bereits registriert."

#: classes.py:350
#, python-format
msgid "Installing package: %s... "
msgstr "Installiere Paket: %s... "

#: classes.py:357
msgid "Already installed."
msgstr "Bereits installiert."

#: classes.py:362 classes.py:369 classes.py:375
msgid "Complete."
msgstr "Vollständig."

#: classes.py:402
msgid "Installed and correct version"
msgstr "Installierte und korrekte Version"

#: classes.py:404
msgid "Missing or incorrect version"
msgstr "Fehlende oder inkorrekte Version"

#: classes.py:441
msgid "None"
msgstr "Keine"

#: classes.py:450
msgid "Not specified"
msgstr "Nicht spezifiziert"

#: classes.py:453
msgid "Patching files... "
msgstr "Dateien werden gepatcht ..."

#: classes.py:481
msgid "Executables that are called directly by the code."
msgstr "Programme, die direkt durch den Code aufgerufen werden."

#: classes.py:483
msgid "Binary"
msgstr "Binärdatei"

#: classes.py:500
msgid ""
"JavaScript libraries downloaded the from NPM registry and used for front-end"
" functionality."
msgstr ""
"JavaScript Bibliotheken, die aus der NPM Registry heruntergeladen werden und"
" für die Front-End-Funktionalität benötigt werden."

#: classes.py:503
msgid "JavaScript"
msgstr "JavaScript"

#: classes.py:535 classes.py:814
msgid "Downloading... "
msgstr "Herunterladen..."

#: classes.py:540
msgid "Verifying... "
msgstr "Verifizieren..."

#: classes.py:545 classes.py:819
msgid "Extracting... "
msgstr "Entpacken..."

#: classes.py:751
msgid "Python packages downloaded from PyPI."
msgstr "Pythonpakete, die von PyPI heruntergeladen werden."

#: classes.py:753
msgid "Python"
msgstr "Python"

#: classes.py:794
msgid "Fonts downloaded from fonts.googleapis.com."
msgstr "Von fonts.googleapis.com heruntergeladene Schriftarten."

#: classes.py:796
msgid "Google font"
msgstr "Google-Schriftart"

#: classes.py:892
msgid "Declared in app"
msgstr "In App deklariert"

#: classes.py:893
msgid "Show dependencies by the app that declared them."
msgstr "Abhängigkeiten nach der deklarierenden App anzeigen."

#: classes.py:897
msgid "Class"
msgstr "Klasse"

#: classes.py:898
msgid ""
"Show the different classes of dependencies. Classes are usually divided by "
"language or the file types of the dependency."
msgstr ""
"Unterschiedliche Klassen von Abhängigkeiten anzeigen. Klassen sind "
"üblicherweise unterteilt nach Sprache oder Dateityp der Abhängigkeit."

#: classes.py:903
msgid "State"
msgstr "Status"

#: classes.py:904
msgid ""
"Show the different states of the dependencies. True means that the "
"dependencies is installed and is of a correct version. False means the "
"dependencies is missing or an incorrect version is present."
msgstr ""
"Die unterschiedlichen Zustände von Abhängigkeiten anzeigen. True (Wahr) "
"bedeutet, dass eine Abhängigkeit mit der korrekten Version installiert ist. "
"False (Falsch) bedeutet eine fehlende oder inkorrekte Version der "
"Abhängigkeit."

#: classes.py:912
msgid ""
"Dependencies required for an environment might not be required for another. "
"Example environments: Production, Development."
msgstr ""
"Abhängigkeiten für eine Umgebung müssen nicht unbedingt für eine andere "
"erforderlich sein, z. B. Produktion im Vergleich zu Entwicklung."

#: environments.py:17
msgid ""
"Environment used for building distributable packages of the software. End "
"users can ignore missing dependencies under this environment."
msgstr ""
"Umgebung, in der installierbare Pakete der Software erstellt werden können. "
"Endbenutzer können fehlende Abhängigkeiten in dieser Umgebung ignorieren."

#: environments.py:20
msgid "Build"
msgstr "Build"

#: environments.py:24
msgid ""
"Environment used for developers to make code changes. End users can ignore "
"missing dependencies under this environment."
msgstr ""
"Umgebung für Entwickler. Endbenutzer können fehlende Abhängigkeiten in "
"dieser Umgebung ignorieren."

#: environments.py:26
msgid "Development"
msgstr "Entwicklung"

#: environments.py:30
msgid ""
"Environment used for building the documentation. End users can ignore "
"missing dependencies under this environment."
msgstr ""
"Umgebung, in der die Dokumentation erstellt wurde. Endbenutzer können "
"fehlende Abhängigkeiten in dieser Umgebung ignorieren."

#: environments.py:32
msgid "Documentation"
msgstr "Dokumentation"

#: environments.py:36
msgid ""
"Environment used to specify direct documentation depedencies to workaround "
"unpinned or immutable depedency bugs in third party libraries. End users can"
" ignore missing dependencies under this environment."
msgstr ""

#: environments.py:40
msgid "Documentation (override)"
msgstr ""

#: environments.py:44
msgid ""
"Normal environment for end users. A missing dependency under this "
"environment will result in issues and errors during normal use."
msgstr ""
"Normale Umgebung für Endbenutzer. Fehlende Abhängigkeiten werden in dieser "
"Umgebung Fehler im normalen Gebrauch verursachen."

#: environments.py:46
msgid "Production"
msgstr "Produktion"

#: environments.py:50
msgid ""
"Environment used running the test suit to verify the functionality of the "
"code. Dependencies in this environment are not needed for normal production "
"usage."
msgstr ""
"Umgebung für Testläufe zur Verifizierung der Codefunktionalität. "
"Abhängigkeiten dieser Umgebung werden nicht im normalen Produktivbetrieb "
"benötigt."

#: environments.py:53
msgid "Testing"
msgstr "Testing"

#: links.py:14 views.py:25
msgid "Check for updates"
msgstr "Nach Updates suchen"

#: links.py:18
msgid "Groups"
msgstr "Gruppen"

#: links.py:23
msgid "Entries"
msgstr "Einträge"

#: links.py:29
msgid "Details"
msgstr "Details"

#: links.py:33
msgid "Dependencies licenses"
msgstr "Lizenzen von Abhängigkeiten"

#: management/commands/dependencies_check.py:13
msgid "Outputs the dependencies as a comma delimited values list."
msgstr "Gibt die Abhängigkeiten als komma-getrennte Werteliste aus."

#: management/commands/dependencies_generate_requirements.py:14
msgid ""
"Comma separated names of dependencies to exclude from the list generated."
msgstr ""
"Kommagetrennte Liste von Abhängigkeiten, die aus der generierten Liste "
"ausgeschlossen werden."

#: management/commands/dependencies_generate_requirements.py:20
msgid ""
"Comma separated names of dependencies to show in the list while excluding "
"every other one."
msgstr ""
"Kommagetrennte Liste von Abhängigkeiten, die in der generierten Liste  "
"erscheinen sollen, während alle anderen ausgeschlossen werden."

#: management/commands/dependencies_install.py:13
msgid "Process a specific app."
msgstr "Eine bestimmte Applikation ausführen."

#: management/commands/dependencies_install.py:17
msgid "Force installation even if already installed."
msgstr "Installation erzwingen, obwohl bereits installiert."

#: permissions.py:10
msgid "View dependencies"
msgstr "Abhängigkeiten ansehen"

#: views.py:40
#, python-format
msgid "Entries for dependency group: %s"
msgstr "Einträge für Abhängigkeitsgruppe %s"

#: views.py:54 views.py:103
#, python-format
msgid "Group %s not found."
msgstr "Gruppe %s nicht gefunden."

#: views.py:68
msgid "Dependency groups"
msgstr "Abhängigkeitsgruppen"

#: views.py:90
#, python-format
msgid "Dependency group and entry: %(group)s, %(entry)s"
msgstr "Abhängigkeitsgruppe und Eintrag: %(group)s, %(entry)s"

#: views.py:115
#, python-format
msgid "Entry %s not found."
msgstr "Eintrag %s nicht gefunden."

#: views.py:134
msgid "Other packages licenses"
msgstr "Andere Paket-Lizenzen"
