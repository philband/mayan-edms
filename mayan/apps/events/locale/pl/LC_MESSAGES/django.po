# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Translators:
# Przemysław Bodio <przemyslawbodio.dev@gmail.com>, 2023
# mic <winterfall24@gmail.com>, 2023
# Roberto Rosario, 2023
# Wojciech Warczakowski <w.warczakowski@gmail.com>, 2023
# 
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-16 23:30+0000\n"
"PO-Revision-Date: 2023-01-05 02:52+0000\n"
"Last-Translator: Wojciech Warczakowski <w.warczakowski@gmail.com>, 2023\n"
"Language-Team: Polish (https://www.transifex.com/rosarior/teams/13584/pl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pl\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);\n"

#: apps.py:31 events.py:11 links.py:52 links.py:93 permissions.py:6
#: queues.py:7 views/event_views.py:29
msgid "Events"
msgstr "Zdarzenia"

#: apps.py:81 apps.py:116
msgid "Date and time"
msgstr "Data i godzina"

#: apps.py:85 apps.py:119 serializers.py:72
msgid "Actor"
msgstr "Czynnik"

#: apps.py:89 apps.py:124
msgid "Event"
msgstr "Zdarzenie"

#: apps.py:93 apps.py:129 serializers.py:78
msgid "Target"
msgstr "Cel"

#: apps.py:97 apps.py:133
msgid "Action object"
msgstr "Obiekt akcji"

#: apps.py:104 forms.py:10 forms.py:67
msgid "Namespace"
msgstr "Przestrzeń nazw"

#: apps.py:109 forms.py:14 forms.py:71 serializers.py:21 serializers.py:46
msgid "Label"
msgstr "Etykieta"

#: apps.py:138
msgid "Seen"
msgstr "Widziane"

#: apps.py:145
msgid "Object"
msgstr "Obiekt"

#: apps.py:150 models.py:46 models.py:105
msgid "Event type"
msgstr "Typ zdarzenia"

#: classes.py:91
msgid "Event list export to CSV"
msgstr ""

#: classes.py:119
msgid "Events exported."
msgstr ""

#: classes.py:121
#, python-format
msgid ""
"The event list has been exported and is available for download using the "
"link: %(download_url)s or from the downloads area (%(download_list_url)s)."
msgstr ""

#: events.py:15
msgid "Events cleared"
msgstr ""

#: events.py:18
msgid "Events exported"
msgstr ""

#: forms.py:18 forms.py:75
msgid "Subscription"
msgstr "Subskrypcja"

#: forms.py:20 forms.py:77
msgid "No"
msgstr "Nie"

#: forms.py:21 forms.py:78
msgid "Subscribed"
msgstr "Subskrybowany"

#: html_widgets.py:23
msgid "System"
msgstr "System"

#: links.py:55 links.py:100 views/clear_views.py:63
msgid "Clear events"
msgstr ""

#: links.py:59 links.py:107 views/export_views.py:60
msgid "Export events"
msgstr ""

#: links.py:65 models.py:53 views/subscription_views.py:54
msgid "Event subscriptions"
msgstr "Subskrypcje zdarzeń"

#: links.py:71 models.py:113 views/subscription_views.py:169
msgid "Object event subscriptions"
msgstr "Subskrypcje zdarzeń obiektowych"

#: links.py:83
msgid "Mark as seen"
msgstr "Oznacz jako widziane"

#: links.py:87
msgid "Mark all as seen"
msgstr "Oznacz wszystkie jako widziane"

#: links.py:114
msgid "Subscriptions"
msgstr "Subskrypcje"

#: literals.py:12
#, python-format
msgid "Unknown or obsolete event type: %s"
msgstr "Nieznany lub nieaktualny typ zdarzenia: %s"

#: models.py:23 serializers.py:24 serializers.py:49
msgid "Name"
msgstr "Nazwa"

#: models.py:27
msgid "Stored event type"
msgstr "Przechowywany typ zdarzenia"

#: models.py:28
msgid "Stored event types"
msgstr "Przechowywane typy zdarzeń"

#: models.py:42 models.py:68 models.py:101 serializers.py:103
msgid "User"
msgstr "Użytkownik"

#: models.py:52
msgid "Event subscription"
msgstr "Subskrypcja zdarzeń"

#: models.py:72 serializers.py:100
msgid "Action"
msgstr "Akcja"

#: models.py:75
msgid "Read"
msgstr "Przeczytane"

#: models.py:82
msgid "Notification"
msgstr "Powiadomienie"

#: models.py:83 views/notification_views.py:33
msgid "Notifications"
msgstr "Powiadomienia"

#: models.py:112
msgid "Object event subscription"
msgstr "Subskrypcja zdarzeń obiektowych"

#: permissions.py:10
msgid "Clear the events of an object"
msgstr ""

#: permissions.py:13
msgid "Export the events of an object"
msgstr ""

#: permissions.py:16
msgid "View the events of an object"
msgstr ""

#: queues.py:13
msgid "Clear event querysets"
msgstr ""

#: queues.py:17
msgid "Export event querysets"
msgstr ""

#: serializers.py:27
msgid "URL"
msgstr "URL"

#: serializers.py:40
msgid "Event type namespace URL"
msgstr ""

#: serializers.py:43
msgid "ID"
msgstr "ID"

#: serializers.py:75
msgid "Actor content type"
msgstr ""

#: serializers.py:81
msgid "Target content type"
msgstr ""

#: serializers.py:84
msgid "Verb"
msgstr "Słowo"

#: views/clear_views.py:27
msgid ""
"This action is not reversible. The process will be performed in the "
"background. "
msgstr ""

#: views/clear_views.py:51
msgid "Event list clear task queued successfully."
msgstr ""

#: views/clear_views.py:84
#, python-format
msgid "Clear events of: %s"
msgstr ""

#: views/clear_views.py:109
#, python-format
msgid "Clear events of type: %s"
msgstr ""

#: views/event_views.py:25
msgid "Events track actions that have been performed on, to, or with objects."
msgstr ""

#: views/event_views.py:28
msgid "There are no events"
msgstr ""

#: views/event_views.py:49
msgid "There are no events for this object"
msgstr "Brak zdarzeń dla tego obiektu"

#: views/event_views.py:51
#, python-format
msgid "Events for: %s"
msgstr "Zdarzenia dla: %s"

#: views/event_views.py:67
msgid "There are no events of this type"
msgstr ""

#: views/event_views.py:69
#, python-format
msgid "Events of type: %s"
msgstr "Zdarzenia typu: %s"

#: views/export_views.py:27
msgid ""
"The process will be performed in the background. The exported events will be"
" available in the downloads area."
msgstr ""

#: views/export_views.py:47
msgid "Event list export task queued successfully."
msgstr ""

#: views/export_views.py:81
#, python-format
msgid "Export events of: %s"
msgstr ""

#: views/export_views.py:100
#, python-format
msgid "Export events of type: %s"
msgstr ""

#: views/notification_views.py:29
msgid "Subscribe to global or object events to receive notifications."
msgstr ""
"Subskrybuj globalne lub obiektowe zdarzenia, aby otrzymywać powiadomienia."

#: views/notification_views.py:32
msgid "There are no notifications"
msgstr "Brak powiadomień"

#: views/notification_views.py:45
msgid "Mark the selected notification as read?"
msgstr ""

#: views/notification_views.py:60
msgid "Notification marked as read."
msgstr ""

#: views/notification_views.py:72
msgid "Mark all notification as read?"
msgstr ""

#: views/notification_views.py:79
msgid "All notifications marked as read."
msgstr ""

#: views/subscription_views.py:38
#, python-format
msgid "Error updating event subscription; %s"
msgstr "Błąd podczas aktualizacji subskrypcji zdarzenia; %s"

#: views/subscription_views.py:43
msgid "Event subscriptions updated successfully"
msgstr "Subskrypcje zdarzeń zostały zaktualizowane pomyślnie"

#: views/subscription_views.py:117
#, python-format
msgid "Error updating object event subscription; %s"
msgstr "Błąd podczas aktualizowania subskrypcji zdarzenia obiektowego; %s"

#: views/subscription_views.py:123
msgid "Object event subscriptions updated successfully"
msgstr "Subskrypcje zdarzeń obiektowych zostały pomyślnie zaktualizowane"

#: views/subscription_views.py:134
#, python-format
msgid "Event subscriptions for: %s"
msgstr "Subskrypcje zdarzeń dla: %s"

#: views/subscription_views.py:164
msgid ""
"Subscribe to the events of an object to received notifications when those "
"events occur."
msgstr ""

#: views/subscription_views.py:167
msgid "There are no object event subscriptions"
msgstr ""
