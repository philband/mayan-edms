# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Sergey Glita <gsv70@mail.ru>, 2023
# lilo.panic, 2023
# Roberto Rosario, 2023
# Ilya Pavlov <spirkaa@gmail.com>, 2023
# 
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-16 23:30+0000\n"
"PO-Revision-Date: 2023-01-05 02:52+0000\n"
"Last-Translator: Ilya Pavlov <spirkaa@gmail.com>, 2023\n"
"Language-Team: Russian (https://www.transifex.com/rosarior/teams/13584/ru/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ru\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: admin.py:13
msgid "Label"
msgstr "Заголовок"

#: apps.py:49 events.py:6 links.py:21 permissions.py:6 queues.py:7
#: settings.py:12
msgid "File metadata"
msgstr "Метаданные файла"

#: apps.py:100 search.py:12 search.py:23 search.py:34
msgid "File metadata key"
msgstr "Ключ метаданных файла"

#: apps.py:104 search.py:16 search.py:27 search.py:38
msgid "File metadata value"
msgstr "Значение метаданных файла"

#: apps.py:130
msgid "Return the value of a specific file metadata."
msgstr "Возвращает значение метаданных конкретного файла."

#: apps.py:131
msgid "File metadata value of"
msgstr "Значение метаданных файла"

#: dependencies.py:12
msgid ""
"Library and program to read and write meta information in multimedia files."
msgstr ""
"Библиотека и программа для чтения и записи метаданных мультимедийных файлов."

#: drivers/exiftool.py:19
msgid "EXIF Tool"
msgstr "EXIF Tool"

#: drivers/extract_msg.py:15
msgid "Extract msg"
msgstr ""

#: events.py:10
msgid "Document file submitted for file metadata processing"
msgstr "Файл документа отправлен на обработку метаданных файла"

#: events.py:14
msgid "Document file file metadata processing finished"
msgstr "Обработка метаданных файла документа завершена"

#: links.py:27
msgid "Attributes"
msgstr "Атрибуты"

#: links.py:34 links.py:39
msgid "Submit for file metadata"
msgstr "Обработать метаданные файла"

#: links.py:49
msgid "Setup file metadata"
msgstr "Настройки метаданных файла"

#: links.py:55
msgid "File metadata processing per type"
msgstr "Обработать метаданные файла указанного типа"

#: methods.py:41
msgid "get_file_metadata(< file metadata dotted path >)"
msgstr "get_file_metadata(< file metadata dotted path >)"

#: methods.py:44
msgid "Return the specified document file metadata entry."
msgstr "Возвращает указанную запись метаданных файла документа."

#: methods.py:71
msgid "Return the specified document file file metadata entry."
msgstr "Возвращает указанную запись метаданных файла файла документа."

#: model_mixins.py:9
msgid "Attribute count"
msgstr "Количество атрибутов"

#: models.py:17 models.py:96
msgid "Driver"
msgstr "Драйвер"

#: models.py:21
msgid "Document file"
msgstr "Файл документа"

#: models.py:27 models.py:63
msgid "Document file driver entry"
msgstr "Запись драйвера файла документа"

#: models.py:28
msgid "Document file driver entries"
msgstr "Записи драйвера файла документа"

#: models.py:40
msgid "Document type"
msgstr "Тип документа"

#: models.py:44
msgid "Automatically queue newly created documents for processing."
msgstr ""
"Автоматически ставить вновь созданные документы в очередь для обработки "
"метаданных файла."

#: models.py:45
msgid "Auto process"
msgstr ""

#: models.py:51
msgid "Document type settings"
msgstr "Настройки типа документа"

#: models.py:52
msgid "Document types settings"
msgstr "Настройки типов документа"

#: models.py:66
msgid "Name of the file metadata entry."
msgstr "Имя записи метаданных файла."

#: models.py:67
msgid "Key"
msgstr "Ключ"

#: models.py:70
msgid "Value of the file metadata entry."
msgstr "Значение записи метаданных файла."

#: models.py:71
msgid "Value"
msgstr "Значение"

#: models.py:76
msgid "File metadata entry"
msgstr "Запись метаданных файла"

#: models.py:77
msgid "File metadata entries"
msgstr "Записи метаданных файла"

#: models.py:87
msgid "Driver path"
msgstr "Путь к драйверу"

#: models.py:91
msgid "Internal name"
msgstr "Внутреннее имя"

#: models.py:97
msgid "Drivers"
msgstr "Драйверы"

#: permissions.py:10
msgid "Change document type file metadata settings"
msgstr "Изменить настройки метаданных файла для типа документа"

#: permissions.py:15
msgid "Submit document for file metadata processing"
msgstr "Отправить документы на обработку метаданных файла"

#: permissions.py:19
msgid "View file metadata"
msgstr "Просмотр метаданных файла"

#: queues.py:11
msgid "Process document file"
msgstr "Обработка файла документа"

#: settings.py:19
msgid ""
"Set new document types to perform file metadata processing automatically by "
"default."
msgstr ""
"Включить по умолчанию автоматический запуск обработки метаданных файла для "
"вновь создаваемых типов документов."

#: settings.py:26
msgid "Arguments to pass to the drivers."
msgstr "Аргументы, передаваемые в драйверы."

#: views.py:52
msgid ""
"File metadata are the attributes of the document's file. They can range from"
" camera information used to take a photo to the author that created a file. "
"File metadata are set when the document's file was first created. File "
"metadata attributes reside in the file itself. They are not the same as the "
"document metadata, which are user defined and reside in the database."
msgstr ""
"Метаданные файла - это атрибуты файла документа. Они могут варьироваться от "
"информации о камере, использованной для фотографирования, до автора, "
"создавшего файл. Метаданные файла устанавливаются при первом создании файла "
"документа. Атрибуты метаданных файла находятся в самом файле. Они не "
"совпадают с метаданными документа, которые определяются пользователем и "
"хранятся в базе данных."

#: views.py:60
msgid "No file metadata available."
msgstr "Метаданные файла недоступны."

#: views.py:63
#, python-format
msgid "File metadata drivers for: %s"
msgstr "Драйверы метаданных файлов для: %s"

#: views.py:90
msgid ""
"This could mean that the file metadata detection has not completed or that "
"the driver does not support any metadata field for the file type of this "
"document."
msgstr ""
"Это может означать, что обнаружение метаданных файла не завершено или что "
"драйвер не поддерживает какие-либо поля метаданных для типа файла этого "
"документа."

#: views.py:95
msgid "No file metadata available for this driver."
msgstr "Для этого драйвера нет доступных метаданных файла."

#: views.py:99
#, python-format
msgid ""
"File metadata attributes for: %(document_file)s with driver: %(driver)s"
msgstr "Атрибуты метаданных файла для: %(document_file)s, драйвер: %(driver)s"

#: views.py:121
#, python-format
msgid "%(count)d documents files submitted to the file metadata queue."
msgstr ""

#: views.py:124
#, python-format
msgid "%(count)d document file submitted to the file metadata queue."
msgstr ""

#: views.py:133
msgid "Submit the selected document file to the file metadata queue?"
msgid_plural "Submit the selected documents files to the file metadata queue?"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""

#: views.py:164
#, python-format
msgid "Edit file metadata settings for document type: %s"
msgstr "Настройки метаданных файла для типа документа: %s"

#: views.py:175
msgid "Submit all documents of a type for file metadata processing."
msgstr ""
"Отправить все документы указанного типа на обработку метаданных файла."

#: views.py:202
#, python-format
msgid "%(count)d documents added to the file metadata processing queue."
msgstr ""
"%(count)d документов добавлено в очередь на обработку метаданных файла."
