# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Translators:
# Roberto Rosario, 2023
# Marcin Lozynski <mlozynski@wp.pl>, 2023
# Tomasz Szymanowicz <alakdae@gmail.com>, 2023
# Wojciech Warczakowski <w.warczakowski@gmail.com>, 2023
# 
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-16 23:30+0000\n"
"PO-Revision-Date: 2023-01-05 02:53+0000\n"
"Last-Translator: Wojciech Warczakowski <w.warczakowski@gmail.com>, 2023\n"
"Language-Team: Polish (https://www.transifex.com/rosarior/teams/13584/pl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pl\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);\n"

#: apps.py:29 links.py:35 permissions.py:6 queues.py:7
msgid "Statistics"
msgstr "Statystyki"

#: apps.py:38
msgid "Type"
msgstr "Typ"

#. Translators: Schedule here is a noun, the 'schedule' at
#. which the statistic will be updated
#: apps.py:45
msgid "Schedule"
msgstr "Harmonogram"

#: apps.py:51
msgid "Last update"
msgstr "Ostatnia aktualizacja"

#: classes.py:45
msgid "Statistics namespace"
msgstr ""

#: classes.py:162
msgid "Never"
msgstr "Nigdy"

#: classes.py:207
msgid "Doughnut chart"
msgstr ""

#: classes.py:212
msgid "Line chart"
msgstr ""

#: classes.py:217
msgid "Pie chart"
msgstr ""

#: links.py:15
msgid "Namespace details"
msgstr "Szczegóły przestrzeni nazw"

#: links.py:20
msgid "Namespace list"
msgstr "Lista przestrzeni nazw"

#: links.py:25
msgid "Queue"
msgstr "Dodaj do kolejki"

#: links.py:30
msgid "View"
msgstr "Pokaż statystykę"

#: models.py:11
msgid "Slug"
msgstr "Slug"

#: models.py:14
msgid "Date time"
msgstr "Data i godzina"

#: models.py:17
msgid "Data"
msgstr "Dane"

#: models.py:21
msgid "Statistics result"
msgstr "Wynik statystyk"

#: models.py:22
msgid "Statistics results"
msgstr "Wyniki statystyk"

#: permissions.py:10
msgid "View statistics"
msgstr "Przegląd statystyk"

#: queues.py:12
msgid "Execute statistic"
msgstr "Wykonaj statystykę"

#: templates/statistics/renderers/chartjs/base.html:12
msgid "No data available."
msgstr "Brak danych"

#: templates/statistics/renderers/chartjs/base.html:14
#, python-format
msgid "Last update: %(datetime)s"
msgstr "Ostatnia aktualizacja: %(datetime)s"

#: view_mixins.py:19
#, python-format
msgid "Statistic \"%s\" not found."
msgstr "Nie znaleziono statystyki \"%s\"."

#: views.py:24
msgid "Statistics namespaces group statistics into logical units. "
msgstr ""

#: views.py:26
msgid "No statistic namespaces available"
msgstr ""

#: views.py:27
msgid "Statistics namespaces"
msgstr "Przestrzenie nazw statystyk"

#: views.py:50
msgid "Statistics are metrics and chart representations of existing data."
msgstr ""

#: views.py:53
msgid "No statistic available"
msgstr ""

#: views.py:55
#, python-format
msgid "Namespace details for: %s"
msgstr "Szczegóły przestrzeni nazw: %s"

#: views.py:78
#, python-format
msgid "Results for: %s"
msgstr "Wyniki dla: %s"

#: views.py:98
#, python-format
msgid "Queue statistic \"%s\" to be updated?"
msgstr "Dodać statystykę \"%s\" do kolejki w celu aktualizacji?"

#: views.py:106
#, python-format
msgid "Statistic \"%s\" queued successfully for update."
msgstr ""
"Statystyka „%s” została pomyślnie umieszczona w kolejce do aktualizacji."
